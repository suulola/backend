import express, { NextFunction, Request } from 'express'
import morgan from 'morgan'
import router from './router'
import cors from 'cors'
import { protect } from './modules/auth'
import { createNewUser, login } from './handler/user';
import { body } from 'express-validator'
import { handleInputError } from './modules/validation'
import { userSchema } from './validation/auth-validation'
import { Response } from "express";
import { failedResponse } from "./modules/format";

const app = express()

app.use(morgan('dev'))
app.use(express.json()) //allows the client to be able to send us json
app.use(express.urlencoded({ extended: true })) //formats query string as an object
app.use(cors())

// app.use((req, res, next) => {
//   req.body.sample_name = `Suulola`
//   next()
// })


app.get('/', (req, res) => {
  res.status(200)
  res.json({ message: `Hello World ${req.body.sample_name}` })
})


app.use(`/api`, [protect], router)

app.post(`/new`,
  userSchema, handleInputError,
  createNewUser
)

app.post(`/login`,
  userSchema, handleInputError,
  login
)

app.use((err, req: Request, res: Response, next: NextFunction) => {
  if (err.type === 'auth') {
    return res.status(401).json(failedResponse(`unauthorized`))
  }
  if (err.type === 'input') {
    return res.status(400).json(failedResponse(`invalid input`))
  }
  return res.status(500).json(failedResponse(`Ooops...`))
})

export default app
