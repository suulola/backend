import { Request, Response, Router } from 'express'
import { handleInputError } from './modules/validation';
import { createProductSchema, createUpdateSchema } from './validation/product-validation';
import * as Product from './handler/product';
import * as Update from './handler/update';

const router = Router();

// Products
router.get(`/product/`, Product.getProducts)
router.get(`/product/:id`, Product.getOneProduct)
router.post(`/product`, createProductSchema, handleInputError, Product.createProduct)
router.put(`/product/:id`, createProductSchema, handleInputError, Product.updateProduct)
router.delete(`/product/:id`, Product.deleteProduct)


// Update 
router.get(`/update/`, Update.getAllUpdates)
// router.get(`/update/:productId`, Update.getAllUpdatesInAProduct)
router.get(`/update/:id`, Update.getOneUpdate)
router.post(`/update`, createUpdateSchema, handleInputError, Update.createUpdate)
router.put(`/update/:id`, Update.updateUpdate)
router.delete(`/update/:id`, Update.deleteUpdate)


// Update Point
router.get(`/updatepoint/`, () => { })
router.get(`/updatepoint/:id`, () => { })
router.post(`/updatepoint`, () => { })
router.put(`/updatepoint/:id`, () => { })
router.delete(`/updatepoint/:id`, () => { })


export default router;