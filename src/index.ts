import * as dotenv from 'dotenv'

dotenv.config()

import server from './server'
import config from './config'

// Error Handling

process.on('uncaughtException', () => {

})
process.on('unhandledRejection', () => {

})

server.listen(config.port, () => {
  console.log(`Server running on http://localhost:${config.port}`)
})
