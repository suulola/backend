import { Request, Response } from "express";
import { successResponse, serviceErrorHandler, failedResponse } from "../modules/format";
import prisma from "../db";

export const createUpdate = async (req: Request, res: Response) => {
  try {
    const { title, body, productId, user_id, ...rest } = req.body;
    const product = await prisma.product.findUnique({
      where: { id: productId }
    });
    if (!product) {
      return res.json(failedResponse(`Invalid Product ID`, null))
    }
    console.log({ rest })
    const update = await prisma.update.create({
      data: { title, body, productId, belongToId: user_id, ...rest }
    });
    return res.json(successResponse('Created successfully', update))
  } catch (error) {
    return res.json(serviceErrorHandler(req, error))
  }
}

export const getAllUpdates = async (req: Request, res: Response) => {
  try {
    const { user_id } = req.body;

    const updates = await prisma.product.findMany({
      where: { belongToId: user_id },
      include: { updates: true }
    })
    return res.json(successResponse(`Updates product fetched`, updates))
  } catch (error) {
    return res.json(serviceErrorHandler(req, error))
  }
}

export const getAllUpdatesInAProduct = async (req: Request, res: Response) => {
  try {
    const product_id = req.params.productId
    const { user_id } = req.body;

    const updates = await prisma.update.findMany({
      where: { productId: product_id, belongToId: user_id }
    })
    return res.json(successResponse(`Updates product fetched`, updates))
  } catch (error) {
    return res.json(serviceErrorHandler(req, error))
  }
}

export const getOneUpdate = async (req: Request, res: Response) => {
  try {

    const update_id = req.params.id;

    const update = await prisma.update.findUnique({
      where: { id: update_id, },
    })
    return res.json(successResponse('Update fetched', update))
  } catch (error) {
    return res.json(serviceErrorHandler(req, error))
  }
}

export const updateUpdate = async (req: Request, res: Response) => {
  try {
    const { user_id, ...rest } = req.body;
    const product_id: string = req.params.id
    const product = await prisma.update.update({
      where: {
        id: product_id,
      },
      data: rest
    });

    return res.json(successResponse('Updated successfully', product))
  } catch (error) {
    return res.json(serviceErrorHandler(req, error))
  }
}


export const deleteUpdate = async (req: Request, res: Response) => {
  try {
    const update_id: string = req.params.id
    const product = await prisma.update.delete({
      where: {
        id: update_id,
      },
    });

    return res.json(successResponse('Deleted successfully', product))
  } catch (error) {
    return res.json(serviceErrorHandler(req, error))
  }
} 