import { Request, Response } from "express"
import prisma from "../db";
import { hashPassword, createToken, comparePasswords } from '../modules/auth';
import { failedResponse, serviceErrorHandler, successResponse } from "../modules/format";

export const createNewUser = async (req: Request, res: Response) => {
  try {
    const { username, password } = req.body;
    const user = await prisma.user.create({
      data: { username, password: await hashPassword(password) }
    });
    const token = createToken(user);
    return res.json(successResponse('Created User successfully', token))
  } catch (error) {
    return res.json(serviceErrorHandler(req, error))
  }
}

export const login = async (req: Request, res: Response) => {
  try {
    const { username, password } = req.body;
    const user = await prisma.user.findUnique({
      where: { username }
    });
    if (!user) {
      return res.json(failedResponse('User does not exist'));
    }
    const verifyPassword = await comparePasswords(password, user.password);
    if (!verifyPassword) {
      return res.json(failedResponse('Wrong password'));
    }
    const token = createToken(user);
    return res.json(successResponse('Login successful', token))

  } catch (error) {
    return res.json(serviceErrorHandler(req, error))
  }
}