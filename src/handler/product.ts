import { Request, Response } from "express";
import { hashPassword, createToken } from "../modules/auth";
import { successResponse, serviceErrorHandler } from "../modules/format";
import prisma from "../db";

export const createProduct = async (req: Request, res: Response) => {
  try {
    const { name, user_id } = req.body;
    const product = await prisma.product.create({
      data: { name, belongToId: user_id }
    });
    return res.json(successResponse('Created successfully', product))
  } catch (error) {
    return res.json(serviceErrorHandler(req, error))
  }
}

export const getProducts = async (req: Request, res: Response) => {
  try {
    const { user_id } = req.body;
    const user = await prisma.user.findUnique({
      where: { id: user_id },
      include: { products: true }
    })
    return res.json(successResponse('User product fetched', user.products))
  } catch (error) {
    return res.json(serviceErrorHandler(req, error))
  }
}

export const getOneProduct = async (req: Request, res: Response) => {
  try {
    const { user_id } = req.body;
    const product_id = req.params.id;

    const product = await prisma.product.findFirst({
      where: { id: product_id, belongToId: user_id },
    })
    return res.json(successResponse('Product fetched', product))
  } catch (error) {
    return res.json(serviceErrorHandler(req, error))
  }
}

export const updateProduct = async (req: Request, res: Response) => {
  try {
    const { name, user_id } = req.body;
    const product_id: string = req.params.id
    const product = await prisma.product.update({
      where: {
        id_belongToId: {
          id: product_id,
          belongToId: user_id
        }
      },
      data: { name }
    });

    return res.json(successResponse('Updated successfully', product))
  } catch (error) {
    return res.json(serviceErrorHandler(req, error))
  }
}


export const deleteProduct = async (req: Request, res: Response) => {
  try {
    const { user_id } = req.body;
    const product_id: string = req.params.id
    const product = await prisma.product.delete({
      where: {
        id_belongToId: {
          id: product_id,
          belongToId: user_id
        }
      },
    });

    return res.json(successResponse('Deleted successfully', product))
  } catch (error) {
    return res.json(serviceErrorHandler(req, error))
  }
} 