import { Request, Response } from "express";
import { successResponse, serviceErrorHandler } from "../modules/format";
import prisma from "../db";

export const createUpdatePoint = async (req: Request, res: Response) => {
  try {
    // const { name, user_id } = req.body;
    // const product = await prisma.updatePoint.create({
    //   data: { name, belongToId: user_id }
    // });
    return res.json(successResponse('Created successfully', {}))
  } catch (error) {
    return res.json(serviceErrorHandler(req, error))
  }
}

export const getUpdatePoint = async (req: Request, res: Response) => {
  try {
    // const { user_id } = req.body;
    // const user = await prisma.user.findUnique({
    //   where: { id: user_id },
    //   include: { products: true }
    // })
    return res.json(successResponse('User product fetched', {}))
  } catch (error) {
    return res.json(serviceErrorHandler(req, error))
  }
}

export const getOneUpdatePoint = async (req: Request, res: Response) => {
  try {
    // const { user_id } = req.body;
    // const product_id = req.params.id;

    // const product = await prisma.product.findFirst({
    //   where: { id: product_id, belongToId: user_id },
    // })
    return res.json(successResponse('Product fetched', {}))
  } catch (error) {
    return res.json(serviceErrorHandler(req, error))
  }
}

export const updateUpdatePoint = async (req: Request, res: Response) => {
  try {
    // const { name, user_id } = req.body;
    // const product_id: string = req.params.id
    // const product = await prisma.product.update({
    //   where: {
    //     id_belongToId: {
    //       id: product_id,
    //       belongToId: user_id
    //     }
    //   },
    //   data: { name }
    // });

    return res.json(successResponse('Updated successfully', {}))
  } catch (error) {
    return res.json(serviceErrorHandler(req, error))
  }
}


export const deleteUpdatePoint = async (req: Request, res: Response) => {
  try {
    const updatePointId: string = req.params.id
    const updatePoint = await prisma.updatePoint.delete({
      where: {
        id: updatePointId
      },
    });

    return res.json(successResponse('Deleted successfully', updatePoint))
  } catch (error) {
    return res.json(serviceErrorHandler(req, error))
  }
} 