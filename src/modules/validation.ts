import { NextFunction, Request, Response } from "express";
import { validationResult } from "express-validator";
import { failedResponse } from './format';


const formatValidationResult = validationResult.withDefaults({
  formatter: ({ location, msg, param, ...rest }) => {
    return {
      message: `${msg}`,
      ...rest
    }
  }
})

export const handleInputError = (req: Request, res: Response, next: NextFunction) => {
  const errors = formatValidationResult(req);
  if (errors.isEmpty()) {
    next()
    return;
  }
  return res.status(400).json(failedResponse(`Validation failed`, errors.array()))
}