import { Request } from "express"

export const successResponse = (message?: string, data?: any) => {
  return {
    statusCode: 200,
    status: true,
    message: message || "Success",
    data: data || null
  }
}
export const serviceErrorHandler = (req: Request, error?: any) => {
  const { originalUrl, method, ip } = req;
  console.log('warn', `URL:${originalUrl} - METHOD:${method} - IP:${ip} - ERROR:${error}`);
  return {
    statusCode: 500,
    status: false,
    message: "Internal server error",
    data: null
  }
}
export const failedResponse = (message: string, data?: any) => {
  return {
    statusCode: 400,
    status: false,
    message: message || "Failed",
    data: data || null
  }

}

