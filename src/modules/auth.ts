import { NextFunction, Request, Response } from 'express';
import jwt from 'jsonwebtoken'
import bcrypt from 'bcrypt'
import { failedResponse } from './format';

export const createToken = (user): string => {
  const token = jwt.sign({ id: user.id, username: user.username }, process.env.JWT_SECRET);
  return token;
}

export const protect = (req: Request, res: Response, next: NextFunction) => {
  const bearer = req.headers.authorization;
  if (!bearer) {
    return res.status(401).json(failedResponse(`Not authorized`))
  }
  const [, token] = bearer.split(' ')
  if (!token) {
    return res.status(401).json(failedResponse(`No token`))
  }
  try {
    const user: any = jwt.verify(token, process.env.JWT_SECRET);
    req.body.user_id = user.id;
    next()
  } catch (error) {
    return res.status(401).json(failedResponse(`Invalid token`))
  }
}


export const hashPassword = (password: string): Promise<string> => {
  return bcrypt.hash(password, 10)
}

export const comparePasswords = (password: string, hash: string): Promise<boolean> => {
  return bcrypt.compare(password, hash);
}