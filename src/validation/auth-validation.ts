import { checkSchema } from "express-validator";

export const userSchema = checkSchema({
  username: {
    isString: true,
    trim: true,
    errorMessage: `Username must a string`,
    isUppercase: { negated: true },
    isLength: {
      errorMessage: `Username must be at least 5 chars long`,
      options: { min: 5 }
    }
  },
  password: {
    // isStrongPassword: true,
    trim: true,
    errorMessage: `Password is required`,
    isLength: {
      errorMessage: `Password must be at least 6 chars long`,
      options: { min: 6 }
    }
  }
})