import { checkSchema } from "express-validator";

export const createProductSchema = checkSchema({
  name: {
    isString: true,
    trim: true,
    errorMessage: `Product name must a string`,
    isUppercase: { negated: true, errorMessage: "must be in lowercase" },
    isLength: {
      errorMessage: `Product name must be at least 5 chars long`,
      options: { min: 5 }
    }
  }
})

export const createUpdateSchema = checkSchema({
  title: {
    isString: true,
    trim: true,
    errorMessage: `Title must a string`,
  },
  body: {
    isString: true,
    trim: true,
    errorMessage: `Body must a string`,
    isLength: { options: { min: 5 } }
  },
  productId: {
    isString: true,
    trim: true,
    errorMessage: `Product Id must be a string`,
  },
  status: {
    isString: true,
    optional: true,
    trim: true,
    errorMessage: `Version must a string`,
    isIn: {
      options: ['IN_PROGRESS', 'SHIPPED', 'DEPRECATED'],
      errorMessage: `Invalid choice specified`
    }
  },
  asset: {
    isString: true,
    optional: true,
    trim: true,
    errorMessage: `Asset must a string`,
  },
  version: {
    isString: true,
    optional: true,
    trim: true,
    errorMessage: `Asset must a string`,
  },
})