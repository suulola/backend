/*
  Warnings:

  - You are about to drop the column `vesrion` on the `Update` table. All the data in the column will be lost.
  - Added the required column `name` to the `Product` table without a default value. This is not possible if the table is not empty.
  - Added the required column `body` to the `Update` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Product" ADD COLUMN     "name" TEXT NOT NULL;

-- AlterTable
ALTER TABLE "Update" DROP COLUMN "vesrion",
ADD COLUMN     "body" TEXT NOT NULL,
ADD COLUMN     "version" TEXT;
